//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Matthew Rowland on 2/9/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (strong, nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cardMatchingMode;
@property (nonatomic) NSInteger numberOfCardsToMatch;
@property (weak, nonatomic) IBOutlet UILabel *updateLabel;
@end

@implementation CardGameViewController

- (NSInteger)numberOfCardsToMatch
{
    if (!_numberOfCardsToMatch) _numberOfCardsToMatch = 2;
    return _numberOfCardsToMatch;
}

- (IBAction)redeal {
    self.game = nil;
    self.scoreLabel.text = @"Score: 0";
    self.cardMatchingMode.enabled = YES;
    self.game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[self createDeck] numberOfCardsToPlayWith:self.numberOfCardsToMatch];
    [self updateUI];
}

- (IBAction)changeCardMatchingMode:(UISegmentedControl *)sender {
    self.cardMatchingMode.selectedSegmentIndex = sender.selectedSegmentIndex;
    self.numberOfCardsToMatch = self.cardMatchingMode.selectedSegmentIndex + 2;
}

- (CardMatchingGame *)game {
    if (!_game) {
        _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[self createDeck] numberOfCardsToPlayWith:self.numberOfCardsToMatch];
    }
    
    return _game;
}

- (Deck *)createDeck {
    return [[PlayingCardDeck alloc] init];
}

- (IBAction)touchCardButton:(UIButton *)sender {
    self.cardMatchingMode.enabled = NO;
    self.game.numberOfCardsToPlayWith = self.numberOfCardsToMatch;
    NSUInteger chosenButtonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:chosenButtonIndex];
    [self updateUI];
}

- (void)updateUI {
    for (UIButton *cardButton in self.cardButtons) {
        NSUInteger cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.game.score];
        self.updateLabel.text = self.game.status;
    }
}

- (NSString *)titleForCard:(Card *)card {
    return card.isChosen ? card.contents : @"";
}

- (UIImage *)backgroundImageForCard:(Card *)card {
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}
@end
