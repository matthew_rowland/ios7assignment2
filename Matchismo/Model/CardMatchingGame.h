//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Matthew Rowland on 2/17/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "Card.h"

@interface CardMatchingGame : NSObject

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *) deck numberOfCardsToPlayWith:(NSInteger)numberOfCardsToPlayWith;
- (void)chooseCardAtIndex:(NSUInteger)index;
- (Card *)cardAtIndex:(NSUInteger)index;

@property (nonatomic, readonly) NSInteger score;

@property (strong, nonatomic) NSString *status;

@property (nonatomic) NSInteger numberOfCardsToPlayWith;

@end
