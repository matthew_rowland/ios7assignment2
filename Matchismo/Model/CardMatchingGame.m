//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Matthew Rowland on 2/17/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray *cards; // of Card
@end

@implementation CardMatchingGame

- (NSMutableArray *)cards {
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init];
    }
    
    return _cards;
}

- (NSString *)status {
    if (!_status) {
        _status = [[NSString alloc] init];
    }
                   
    return _status;
}

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck numberOfCardsToPlayWith:(NSInteger)numberOfCardsToPlayWith {
    self = [super init];
    
    if (self) {
        self.numberOfCardsToPlayWith = numberOfCardsToPlayWith;
        for (int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            if (card) {
                [self.cards addObject:card];
            }
            else {
                self = nil;
                break;
            }
        }
    }
    
    return self;
}

- (Card *)cardAtIndex:(NSUInteger)index {
    return (index < [self.cards count]) ? self.cards[index] : nil;
}


static const int MATCH_BONUS = 4;
static const int MATCH_PENALTY = 2;
static const int COST_TO_CHOOSE = 1;

- (void)chooseCardAtIndex:(NSUInteger)index
{
    Card *card = [self cardAtIndex:index];
    self.status = [NSString stringWithFormat:@"%@", card.contents];
    
    if (!card.isMatched) {
        if (card.isChosen) {
            card.chosen = NO;
            self.status = @"";
        }
        else {
            NSMutableArray *otherCards = [NSMutableArray array];
            for (Card *otherCard in self.cards) {
                if (otherCard.isChosen && !otherCard.isMatched) {
                    [otherCards addObject:otherCard];
                }
            }
            if ([otherCards count] == self.numberOfCardsToPlayWith - 1) {
                int matchScore = [card match:otherCards];
                if (matchScore) {
                    self.score += matchScore * MATCH_BONUS;
                    card.matched = YES;
                    NSString *otherCardsContents = @"";
                    for (Card *otherCard in otherCards) {
                        otherCard.matched = YES;
                        otherCardsContents = [otherCardsContents stringByAppendingFormat:@"%@", otherCard.contents];
                    }
                    self.status = [NSString stringWithFormat:@"Matched %@ %@ for score: %d", card.contents, otherCardsContents, matchScore * MATCH_BONUS];
                }
                else {
                    self.score -= MATCH_PENALTY;
                    for (Card *otherCard in otherCards) {
                        otherCard.chosen = NO;
                    }
                    self.status = [NSString stringWithFormat:@"Mismatch! Penalty of %d", MATCH_PENALTY];
                }
            }
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES;
        }
    }
}

@end
