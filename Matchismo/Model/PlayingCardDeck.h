//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Matthew Rowland on 2/9/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
