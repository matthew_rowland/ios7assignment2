//
//  main.m
//  Matchismo
//
//  Created by Matthew Rowland on 2/9/14.
//  Copyright (c) 2014 Matthew Rowland. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
